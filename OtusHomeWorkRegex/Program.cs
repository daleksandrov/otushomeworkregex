﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace OtusHomeWorkRegex
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri uri = new Uri("http://edu.rosminzdrav.ru/");

            Match match;

            string pattern = @"href\s*=\s*[""']?(?<url>[^""'>\s]+)[""'>\s]";

            try
            {
                string html = new WebClient().DownloadString(uri);
                match = Regex.Match(html, pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

                Console.WriteLine("Found:");

                while(match.Success)
                {
                    Console.WriteLine($"{match.Groups["url"]} at {match.Groups["url"].Index}");
                    match = match.NextMatch();
                }
            }
            catch(WebException ex)
            {
                Console.WriteLine("WebException: " + ex.Message);
            }
            catch(RegexMatchTimeoutException)
            {
                Console.WriteLine("RegexMatchTimeoutException");
            }

        }
    }
}
